<?php

function deployKeys($data){
	
	$result = array();

	foreach ($data as $key => $value) {
		
		$keys = explode('.', $key);
		$countKeys = count($keys) - 1;

		foreach($keys as $subKey => $keyElement){

			if(!isset($tmp)){
				$tmp = &$result;
			}

			if($countKeys == $subKey){
				$tmp[$keyElement] = $value;
				unset($tmp);
			}else{

				if(!isset($tmp[$keyElement])){
					$tmp[$keyElement] = array();
				}

				$tmp = &$tmp[$keyElement];
			}
		}

	}
}


$data = array(
	'element1.child.property1' => 1,
	'element1.child.property2' => 2,
	'element2.child.name' => 3,
	'element2.child2.name' => 4,
	'element2.child2.position' => 5,
	'element3.child3.position' => 6
);

deployKeys($data);

/* Дано 1С-Bitrix Cтарт с бесконечным количеством шаблонов сайтов.
Задача: Подключить на всех шаблонах сайтов /addScript.js. Описать
решение задачи. 
Если скрипт нужен на одной странице сайтов, можно подключить в файле вызова компонента
Если скрипт нужен на нескольких страницах, думаю можно подключить его в событии onEpilog через addJs или AddHeadScript 
*/
